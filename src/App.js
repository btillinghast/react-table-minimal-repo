import React, { Component } from 'react';
import MyTable from './MyTable/MyTable';

class App extends Component {
  render() {
    return (
        <MyTable />
    );
  }
}

export default App;
